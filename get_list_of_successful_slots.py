from selenium import webdriver
from selenium.webdriver.common.by import By

def get_successful_slots(cells, timeslots):
    successful_slots = []
    for cell in cells:
        cell_class = cell[2].get_attribute("class")
        tennis_check = "Tennis" in cell[0]
        active_check = cell_class == "grid-cell"
        timeslot_check = cell[1] in timeslots
        if tennis_check and active_check and timeslot_check:
            successful_slots.append(cell)
    return successful_slots
