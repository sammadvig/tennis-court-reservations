from selenium import webdriver
from selenium.webdriver.common.by import By

def click_submit(driver: webdriver):
    buttons = driver.find_elements(By.TAG_NAME, "button")
    for button in buttons:
        if button.text == "Confirm bookings":
            button.click()
            break


