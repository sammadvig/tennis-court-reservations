from private import (
    name,
    card_number,
    expiration_month,
    expiration_year,
    cvc
)
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from search_again import search_again_single
from selenium.webdriver.support.select import Select


def enter_cc_details(driver):
    # sleep(10)
    # cc_form = search_again_single(driver, By.TAG_NAME, "form")
    # print(cc_form)
    WebDriverWait(driver, 10).until(
        EC.frame_to_be_available_and_switch_to_it((
            By.CSS_SELECTOR,
            'iframe[title="primaryPCIPaymentIframe"]'
            ))
        )
    form_container = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.TAG_NAME, "form"))
    )
    inputs = form_container.find_elements(By.TAG_NAME, "input")
    sleep(1)
    if len(inputs) == 4:
        inputs[0].send_keys(name)
        inputs[1].send_keys(card_number)
        inputs[2].send_keys(cvc)
    else:
        print("oops, didn't select inputs correctly")
    selects = form_container.find_elements(By.TAG_NAME, "select")
    print(len(selects))
    select_month = Select(selects[0])
    select_year = Select(selects[1])
    select_month.select_by_value(expiration_month)
    select_year.select_by_value(expiration_year)
    # selects[0].send_keys(expiration_month)
    # selects[1].send_keys(expiration_year)
