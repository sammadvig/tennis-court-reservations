from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

def search_again_single(driver: webdriver, by_type, query, i=0):
    try:
        element = driver.find_element(by_type, query)
        return element
    except NoSuchElementException:
        if(i < 10):
            i += 1
            search_again_single(driver, by_type, query, i)
        else:
            raise NoSuchElementException

def search_again_multiple(driver: webdriver, by_type, query, i=0):
    try:
        elements = driver.find_elements(by_type, query)
        return elements
    except NoSuchElementException:
        if(i < 10):
            i += 1
            search_again_single(driver, by_type, query, i)
        else:
            raise NoSuchElementException
