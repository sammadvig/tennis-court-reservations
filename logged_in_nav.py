from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def logged_in_nav(driver:webdriver):
    # Wait until fully logged in and navigated to the next page
    # Otherwise, selenium will get the element from the log in page,
    # and the element will be stale
    WebDriverWait(driver, 10).until(EC.title_is("View Account | CPD Transaction Portal"))

    reservations_element = driver.find_element(By.LINK_TEXT, "Reservations")
    reservations_element.click()

    court_css_selector = 'a[aria-label="Park Rental - McFetridge Indoor Tennis/Pickleball"]'
    court_link = driver.find_element(By.CSS_SELECTOR, court_css_selector)
    court_link.click()
