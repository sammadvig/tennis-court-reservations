from datetime import date, timedelta

def get_list_of_timeslots():
    current_date = date.today()
    future_date = current_date + timedelta(days=6)
    # weekdays = ["Mon", "Tue", "Wed", "Thu"]
    friday = ["Fri"]
    weekends = ["Sat", "Sun"]

    weekday_times = ["6:00 PM"]
    friday_times = ["12:00 PM"]
    weekend_times = [
        "11:00 AM",
        "12:00 PM",
        "1:00 PM",
        "2:00 PM"
        ]

    day_of_week = future_date.strftime("%a")
    if day_of_week in friday:
        return friday_times
    elif day_of_week in weekends:
        return weekend_times
    else:
        return weekday_times

