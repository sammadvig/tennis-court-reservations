from datetime import date, timedelta
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def navigate_to_six_days_away(driver: webdriver):
    current_date = date.today()
    future_date = current_date + timedelta(days=6)

    calendar_element_selector = 'input[aria-label="Date picker, current date"]'
    calendar = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((
            By.CSS_SELECTOR,
            calendar_element_selector
            ))
    )
    # future_date_string = future_date.strftime("%b %-d, %Y")
    future_date_month = future_date.strftime("%b")
    future_date_day = str(int(future_date.strftime("%d")))
    future_date_year = future_date.strftime("%Y")
    future_date_string = future_date_month + " " + future_date_day + ", " + future_date_year

    calendar.click()

    popup_cal_selector = 'div[data-qa-id="popupCalendar"]'
    popup_cal = driver.find_element(By.CSS_SELECTOR, popup_cal_selector)
    print("popup_cal:", popup_cal)

    if current_date.month != future_date.month:
        chev_right = popup_cal.find_element(By.CLASS_NAME, "icon-chevron-right")
        print("chev_right:", chev_right)
        chev_right.click()
    cal_table = popup_cal.find_element(By.TAG_NAME, "table")
    cal_table_body = cal_table.find_element(By.TAG_NAME, "tbody")
    cal_table_rows = cal_table_body.find_elements(By.TAG_NAME, "tr")


    for row in cal_table_rows:
        if row.get_attribute("class") == "an-calendar-table-row":
            row_tds = row.find_elements(By.TAG_NAME, "td")
            for td in row_tds:
                date_str = td.find_element(By.TAG_NAME, "div").get_attribute("aria-label")
                if date_str == future_date_string:
                    td.click()
                    break
            else:
                continue
            break
