from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def find_available_timeslots(driver: webdriver):
    WebDriverWait(driver, 10).until(
        EC.title_is("Quick reservation | CPD Transaction Portal")
        )
    # print("Quick reservation | CPD Transaction Portal")
    # tables = driver.find_elements(By.CSS_SELECTOR, "table")
    table = WebDriverWait(driver, 20).until(
    EC.presence_of_element_located((By.CSS_SELECTOR, 'table'))
    )
    print(table)
    # Getting a dictionary of index values and timeslots associated so we can
    # select table elements by preferred timeslots
    thead = table.find_element(By.TAG_NAME, "thead")
    thead_row = thead.find_element(By.TAG_NAME, "tr")
    thead_headers = thead_row.find_elements(By.TAG_NAME, "th")
    time_dict = {}
    for i in range(1,len(thead_headers)):
        elem = thead_headers[i]
        time_dict[i] = elem.find_element(By.TAG_NAME, "div").text

    element_detail_list = []

    tbody = table.find_element(By.TAG_NAME, "tbody")
    t_rows = tbody.find_elements(By.TAG_NAME, "tr")

    for row in t_rows:
        spans = row.find_elements(By.TAG_NAME, "span")
        row_title = spans[3].text
        cells = row.find_elements(By.TAG_NAME, "td")
        for i in range(len(cells)):
            index = i + 1
            timeslot = time_dict[index]
            cell_object = cells[i].find_element(By.TAG_NAME, "div")
            detail_list = [row_title, timeslot, cell_object]
            element_detail_list.append(detail_list)

    return element_detail_list
