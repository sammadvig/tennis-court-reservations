from selenium import webdriver
import random

def click_random_timeslot(driver: webdriver, timeslot_list: list):
    if len(timeslot_list) > 0:
        random_choice = random.choice(timeslot_list)
        random_object = random_choice[2]
        random_object.click()
    else:
        print("No Slots Available")
