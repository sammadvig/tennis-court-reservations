from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def reserve(driver: webdriver):
    button_selector = 'button[data-qa-id="quick-reservation-reserve-button"]'

    button = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, button_selector))
    )
    button.click()

    waiver_container = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "waiver-panel"))
    )

    ok_button = waiver_container.find_element(By.TAG_NAME, "button")
    ok_button.click()
