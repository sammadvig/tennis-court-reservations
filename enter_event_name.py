from selenium import webdriver
from selenium.webdriver.common.by import By

def enter_event_name(driver: webdriver):
    input_field = driver.find_element(
        By.CSS_SELECTOR,
        'input[aria-label="Event name"]'
        )
    input_field.send_keys("Tennis")
