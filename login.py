
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from private import website_login_email, website_login_password


def login(driver:webdriver):
    driver.get('https://anc.apm.activecommunities.com/chicagoparkdistrict/signin')

    email_component = 'input[aria-label="Email address Required"]'
    password_component = 'input[aria-label="Password Required"]'
    button_component = 'button[type="submit"]'

    email_input = driver.find_element(By.CSS_SELECTOR, email_component)
    password_input = driver.find_element(By.CSS_SELECTOR, password_component)
    button = driver.find_element(By.CSS_SELECTOR, button_component)

    email_input.send_keys(website_login_email)
    password_input.send_keys(website_login_password)
    button.click()
