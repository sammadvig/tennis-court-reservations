from selenium import webdriver
from login import login
from logged_in_nav import logged_in_nav
from get_to_right_day import navigate_to_six_days_away
from find_available_timeslots import find_available_timeslots
from get_time_prefs_by_date import get_list_of_timeslots
from get_list_of_successful_slots import get_successful_slots
from enter_event_name import enter_event_name
from click_random_timeslot import click_random_timeslot
from submit_request import click_submit
from handle_disclaimer import handle_disclaimer
from reserve import reserve
from enter_details import enter_cc_details
# from private import okay_times


driver = webdriver.Chrome()
login(driver)
logged_in_nav(driver)
navigate_to_six_days_away(driver)
cell_details = find_available_timeslots(driver)
# print(get_successful_slots(cell_details, get_list_of_timeslots()))
# print(get_successful_slots(cell_details, ["6:00 AM"]))


# slots for testing
# successful_slots = get_successful_slots(cell_details, okay_times)


# official preferred slots
successful_slots = get_successful_slots(cell_details, get_list_of_timeslots())


enter_event_name(driver)
click_random_timeslot(driver, successful_slots)
click_submit(driver)
handle_disclaimer(driver)
reserve(driver)
enter_cc_details(driver)

print("Loading")
