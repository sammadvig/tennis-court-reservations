from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def handle_disclaimer(driver: webdriver):
    disclaimer = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CLASS_NAME, "disclaimer"))
    )
    check_box = disclaimer.find_element(By.TAG_NAME, "input")
    check_box.click()
    buttons = driver.find_elements(By.TAG_NAME, "button")
    for button in buttons:
        if button.text == "Save":
            button.click()
            break
